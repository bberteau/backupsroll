##!/usr/bin/python
# -*- coding: utf-8 -*-
# vim: tabstop=8 expandtab shiftwidth=4 softtabstop=4

# Python > 2.6, argparse, rsync

import os
import sys
import argparse
from shutil import rmtree
from datetime import date

dossiers = []
date_courante = date.today().strftime("%Y-%m-%d")
rsync_args = ""

parser = argparse.ArgumentParser(description='Rolling backups in different dated directories', fromfile_prefix_chars='@')
parser.add_argument('-s', '--source', action="store", dest="source", default="", help="Source directory", required=True)
parser.add_argument('-t', '--target', action="store", dest="target", default="", help="Target directory", required=True)
parser.add_argument('-n', action="store", dest="backups_number", default=2, type=int, help="Number of backups to keep (>=2)")
parser.add_argument('-e', action="append", dest="exclude_pattern", default=[], help="Exclude files matching pattern (rysnc)")
parser.add_argument('--exclude-file', action="store", dest="exclude_file", default="", help="Exclude from file (rsync)")
parser.add_argument('--delete', dest="delete", action="store_true", default=False, help="Delete extraneous files from dest dirs (rsync)")
parser.add_argument('--delete-excluded', dest="delete_excluded", default=False, action="store_true", help="Also delete excluded files from dest dirs (rsync)")
parser.add_argument('--max-size', action="store", dest="max_size", type=str, default="", help="Don't transfer any file larger than max_size (rsync)")
parser.add_argument('--min-size', action="store", dest="min_size", type=str, default="", help="Don't transfer any file smaller than min_size (rsync)")
# parser.add_argument('--rsync-args', action="store", default="", type=str, help="arguments to pass to rsync. Needs to quote the args.")
parser.add_argument('--dry-run', action="store_true", default=False, dest="dryrun", help="Perform a trial run with no changes made")
parser.add_argument('--version', action='version', version='%(prog)s 0.1')

results = parser.parse_args()
source = os.path.normpath(results.source)
target = os.path.normpath(results.target)
nb_backups = results.backups_number

# ajout des paramêtres rsync
if results.delete:
    rsync_args += " --delete"
if results.dryrun:
    rsync_args += " --dry-run"
if results.delete_excluded:
    rsync_args += " --delete-excluded"
if results.max_size:
    rsync_args += " --max-size {}".format(results.max_size)
if results.min_size:
    rsync_args += " --min-size {}".format(results.min_size)
if results.exclude_pattern:
    for pattern in results.exclude_pattern:
        rsync_args += " -e {}".format(pattern)
if results.exclude_file:
    if os.path.exists(results.exclude_file):
        rsync_args += " --exclude-from={}".format(results.exclude_file)
    else:
        print("le fichier {} n'existe pas".format(results.exclude_file))
        sys.exit(-2)

rsync = "/usr/bin/rsync -av {} {} ".format(rsync_args, source)

# tester source!=destination et destination non incluse dans source et inversement.

# Vérification des chemins
for dossier in (source, target):
    if not os.path.exists(dossier):
        print("**** {} n'existe pas ****".format(dossier))
        print("**** merci de vérifier votre configuration !")
        sys.exit(-2)

if not os.listdir(source):
    # On vérifie que le dossier source n'est pas vide
    print("Le dossiers source {} est vide".format(source))
    print("La sauvegarde ne sera pas exécutée")
    sys.exit(-2)
else:
    dossiers = [ os.path.normpath(target+os.sep+dossier) for dossier in os.listdir(target) if os.path.isdir(target+os.sep+dossier)]

dossiers.sort()

# On garde au minimum 2 sauvegardes sinon un simple rsync suffit
nb_backups = nb_backups if nb_backups > 2 else 2

# Nom du dossier qui va être créé
nouveau = os.path.normpath(target+os.sep+date_courante)

# On vérifie le nombre de dossiers présents dans le reportoire de sauvegarde et on supprime éventuellement les plus anciens
if len(dossiers) >= nb_backups:
    # suppression des dossiers en trop
    while len(dossiers) > nb_backups:
        # os.rmdir(dossiers.pop(0))
        rmtree(dossiers.pop(0))
    if os.path.exists(nouveau):
        if not os.path.isdir(nouveau):
            print("**** {} existe déjà et n'est pas un dossier".format(nouveau))
            exit(-2)
        else:
            print("Le dossier {} existe déjà !".format(nouveau))
            print("Son contenu va être écrasé par la nouvelle sauvegarde")
    else:
        os.rename(dossiers[0], nouveau)
else:
    if os.path.exists(nouveau):
        if not os.path.isdir(nouveau):
            print("**** {} existe déjà et n'est pas un dossier".format(nouveau))
            exit(-2)
    else:
        # Création du dossier daté du jour
        os.mkdir(nouveau)

rsync += "{}/".format(nouveau)
print("Synchronisation du dossier :\n* {}".format(rsync))
os.system(rsync)
