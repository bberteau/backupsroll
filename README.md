#Backupsroll

Rotation de sauvegardes sur plusieurs jours dans des dossiers datés
Utilise rsync pour la sauvegarde

Usage:  

```
backupsroll.py  [-h] -s SOURCE -t TARGET [-n backup_nb]  
                    [-e EXCLUDE_PATTERN] [--exclude-file EXCLUDE_FILE]  
                    [--delete] [--delete-excluded] [--max-size MAX_SIZE]  
                    [--min-size MIN_SIZE] [--dry-run] [--version]  

optional arguments:  
    -h, --help                  Show this help message and exit  
    -s SOURCE, --source SOURCE  Source directory  
    -t TARGET, --target TARGET  Target directory  
    -n backups_number           Number of backups to keep (>=2)  
    -e EXCLUDE_PATTERN          Exclude files matching pattern (rysnc)  
    --exclude-file EXCLUDE_FILE Exclude from file (rsync)  
    --delete                    Delete extraneous files from dest dirs (rsync)  
    --delete-excluded           Also delete excluded files from dest dirs (rsync)  
    --max-size MAX_SIZE         Don't transfer any file larger than max_size (rsync)  
    --min-size MIN_SIZE         Don't transfer any file smaller than min_size (rsync)  
    --dry-run                   Perform a trial run with no changes made  
    --version                   Show program's version number and exit  
```

Le paramètres entre crochets sont optionnels.

Pour plus d'informations sur les paramètres où est indiqué "rsync", se référer à l'aide de rsync.

Exemple d'utilisation :

`python backupsroll.py -s /home/bruno/documents/ -t /mnt/SAUVEGARDES/bruno/docs -n 3 --delete-excluded --delete --exclude-file exclude-backup.txt`

On peut aussi passer les paramètres par un fichier : `python backupsroll.py @backup-doc.cnf`


Sauvegarde du dossier ~/documents sur 3 jours (à condition que le script soit exécuté quotidiennement).  
La liste des fichiers à exclure est fournie par exclude-backup.txt et seront effacés des sauvegardes.  
Des dossiers datés sont crées lors des n premières utilisations dans le dossier destination ou si on augmente le nombre de sauvegardes à conserver.  
Ensuite le dossier le plus ancien est renommé pour effectuer la sauvegarde en cours et synchronisé via rsync.  
Si on diminue le paramètre n le script supprime les dossiers surnuméraires en commençant par le plus ancien jusqu'à n'avoir plus que le nombre de dossiers désiré.  

**Attention :** le dossier destination ne doit contenir que les sauvegardes car les autres dossiers pourront être effacés selon les options choisies.

Ce qui donne dans notre exemple:  

J:  
/mnt/sauvegardes/bruno/docs/  
    2018-01-27/  

J+1:  
/mnt/sauvegardes/bruno/docs/  
    2018-01-27/  
    2018-01-28/  

J+2:  
/mnt/sauvegardes/bruno/docs/  
    2018-01-27/  
    2018-01-28/  
    2018-01-29/  

J+3:  
/mnt/sauvegardes/bruno/docs/  
    2018-01-28/  
    2018-01-29/  
    2018-01-30/ (le dossier 2018-01-27 a été renommé et mis à jour avec rsync)  
